package Poker;

import Cards.Deck;
import Hand.Hand;
import Hand.HandType;
import Hand.InvalidHandSizeException;

/**
 * Poker Class
 * Deals cards from the deck into a players hand and checks to see what hand it is
 */
public class Poker {

    private final int handSize;
    private Deck deck;
    private Hand playerHand;

    /**
     * @param handSize The size of the hand to be dealt
     * @throws InvalidHandSizeException Thrown if hand size is larger than deck size
     */
    public Poker(int handSize) throws InvalidHandSizeException {
        deck = new Deck();
        playerHand = new Hand();

        if(handSize >= deck.getDeckLimit()) {
            throw new InvalidHandSizeException("Hand size is larger than deck size");
        }

        if(handSize < 1) {
            throw new InvalidHandSizeException("Hand size is smaller than 1");
        }

        this.handSize = handSize;
    }

    /**
     * Remove cards from the deck and add it to the hand depending on hand size
     */
    public void dealCards() {
        for(int i = 0; i < handSize; i++) {
            playerHand.add(deck.drawCard());
        }
    }

    /**
     * @return Hand Returns the player's hand
     */
    public Hand getPlayerHand() {
        return playerHand;
    }

    /**
     * Play a game of poker
     * Shuffle, deal, then check hand
     */
    public void play() {
        dealCards();

        HandType handType = playerHand.checkHandType();

        System.out.println(playerHand);
        System.out.println("You have: " + handType);
    }
}
