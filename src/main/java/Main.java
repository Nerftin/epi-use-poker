import Hand.InvalidHandSizeException;
import Poker.Poker;

public class Main {

    public static void main(String[] args) {
        try {
            // Start a new poker game with a hand size of five
            Poker poker = new Poker(5);
            poker.play();
        } catch (InvalidHandSizeException e) {
            e.printStackTrace();
        }
    }
}
