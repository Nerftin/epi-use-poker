package Cards;

/**
 * Represents a card face
 */
public enum Face {

    TWO(1, "2"), THREE(2, "3"), FOUR(3, "4"), FIVE(4, "5"),
    SIX(5, "6"), SEVEN(6, "7"), EIGHT(7, "8"), NINE(8 , "9"),
    TEN(9, "10"), JACK(10, "J"), QUEEN(11, "Q"), KING(12,"K"),
    ACE(13,"A");

    private final int rank;
    private final String symbol;

    /**
     * @param rank The value of the card
     * @param symbol The face of the card
     */
    Face(int rank, String symbol) {
        this.rank = rank;
        this.symbol = symbol;
    }

    /**
     * @return int Returns the rank of the card
     */
    public int getRank() {
        return rank;
    }

    @Override
    public String toString() {
        return symbol;
    }
}
