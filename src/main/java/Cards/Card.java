package Cards;

/**
 * Represents a card
 */
public class Card implements Comparable<Card> {

    private final Face face;
    private final Suit suit;

    /**
     * @param face Card face
     * @param suit Card suit
     */
    public Card(Face face, Suit suit) {
        this.face = face;
        this.suit = suit;
    }

    /**
     * @return Face Returns the face of the card
     */
    public Face getFace() {
        return face;
    }

    /**
     * @return Suit Returns the suit of the card
     */
    public Suit getSuit() {
        return suit;
    }

    /**
     * @return Returns a string representing the card
     */
    @Override
    public String toString() {
        return face.toString() + suit.toString();
    }

    /**
     * Compares card ranks, used for sorting deck
     *
     * @param c Card to compare to
     * @return int Comparison result
     */
    @Override
    public int compareTo(Card c) {
        if(c.face.getRank() < this.face.getRank()) {
            return 1;
        } else if(c.face.getRank() > this.face.getRank()) {
            return -1;
        } else {
            return  1;
        }
    }

    /**
     * Hash is simply the rank since that is all what is needed to determine the hand type
     *
     * @return int Hash of the card
     */
    @Override
    public int hashCode() {
        return this.getFace().getRank();
    }

    /**
     * Compares two cards only on the face rank
     * Does not care about suit
     *
     * @param o Object to compare
     * @return boolean Returns true if cards are the same
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (this.getClass() != o.getClass()) {
            return false;
        }

        Card other = (Card) o;
        if (this.getFace().getRank() != other.getFace().getRank()) {
            return false;
        }

        return true;
    }
}
