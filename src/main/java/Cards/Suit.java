package Cards;

/**
 * Represents a card suit
 */
public enum Suit {

    CLUB('\u2663'), DIAMOND('\u2666'), HEART('\u2665'), SPADE('\u2660');

    private final char symbol;

    /**
     * @param symbol Character representing the suit
     */
    Suit(char symbol) {
        this.symbol = symbol;
    }

    /**
     * @return char returns the suit symbol
     */
    public char getSymbol() {
        return symbol;
    }

    /**
     * @return String Returns a string of the character representing the suit
     */
    @Override
    public String toString() {
        return String.valueOf(symbol);
    }
}
