package Cards;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Represents a deck of cards
 */
public class Deck {

    private ArrayList<Card> deck;
    private int deckLimit;

    public Deck() {
        createDeck();
    }

    /**
     * Create a new deck of cards based.
     */
    private void createDeck() {
        deck = new ArrayList<>();

        // Create a card for every combination of faces and suites
        // and add it to the deck
        for(Face face: Face.values()) {
            for(Suit suit: Suit.values()) {
                this.deck.add(new Card(face, suit));
            }
        }

        deckLimit = deck.size();
        shuffle();
    }

    /**
     * @return int size of the deck
     */
    public int size() {
        return deck.size();
    }

    /**
     * @return ArrayList<Card> returns the deck
     */
    public ArrayList<Card> getDeck() {
        return deck;
    }

    /**
     * @return int returns the size of the deck when full
     */
    public int getDeckLimit() {
        return deckLimit;
    }

    /**
     * Remove a card from the deck and deal it
     *
     * @return Card Returns a card at index 0 of the deck
     */
    public Card drawCard() {
        return deck.remove(0);
    }

    /**
     * Randomly shuffle the deck of cards
     */
    public void shuffle() {
        Collections.shuffle(deck);
    }
}
