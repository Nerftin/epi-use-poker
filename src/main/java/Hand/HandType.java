package Hand;

import Hand.Check.*;

public enum HandType {
    STRAIGHT_FLUSH(9, "Straight Flush", new StraightFlushCheck()),
    FOUR_OF_A_KIND(8, "Four of a Kind", new FourOfAKindCheck()),
    FULL_HOUSE(7, "Full House", new FullHouseCheck()),
    FLUSH(6, "Flush", new FlushCheck()),
    STRAIGHT(5, "Straight", new StraightCheck()),
    THREE_OF_A_KIND(4, "Three of a Kind", new ThreeOfAKindCheck()),
    TWO_PAIR(3, "Two Pair", new TwoPairCheck()),
    ONE_PAIR(2, "One Pair", new OnePairCheck()),
    HIGH_CARDS(1, "High Cards", new HighCardCheck());

    private final int rank;
    private final String name;
    private final HandCheck handCheck;

    /**
     * @param rank The value of the hand
     * @param name The name of the hand
     * @param handCheck Method to check if it is the hand
     */
    HandType(int rank, String name, HandCheck handCheck) {
        this.rank = rank;
        this.name = name;
        this.handCheck = handCheck;
    }

    /**
     * @param hand player's hand
     * @return boolean returns true if player's hand matches the hand
     */
    public boolean check(Hand hand) {
        return handCheck.check(hand);
    }

    /**
     * @return int Returns the rank of the hand
     */
    public int getRank() {
        return rank;
    }

    /**
     * @return String Returns the name of the hand
     */
    @Override
    public String toString() {
        return this.name;
    }


}
