package Hand;

import Cards.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a player's hand
 */
public class Hand {
    private final ArrayList<Card> hand;
    private Map<Card, Integer> handMap;
    private boolean handMapUpToDate;

    public Hand() {
        this.hand = new ArrayList<>();
        this.handMapUpToDate = false;
    }

    /**
     * @param card Card to add to the hand
     */
    public void add(Card card) {
        hand.add(card);
        handMapUpToDate = false;
    }

    /**
     * @return ArrayList<Card> Returns the hand
     */
    public ArrayList<Card> getHand() {
        return hand;
    }

    /**
     * Create a hash map showing how many of each card is in a hand
     * keys are card rank and values are number of cards
     */
    private void createHandMap() {
        handMapUpToDate = true;
        handMap = new HashMap<>();

        // Add each card in hand to hash map and increment count
        for (Card c : hand) {
            Integer i = handMap.get(c);
            handMap.put(c, (i == null) ? 1 : i + 1);
        }
    }

    /**
     * Creates, if needed a hashmap of the hand and returns it
     *
     * @return Map<Card, Integer> Returns a hashmap of the hand
     */
    public Map<Card, Integer> getHandMap() {
        if(!handMapUpToDate) {
            createHandMap();
        }

        return handMap;
    }

    /**
     * Determine the hand
     *
     * @return HandTypes Returns the type of hand
     */
    public HandType checkHandType() {
        HandType handType = null;

        // For all hand types, check the current hand against them
        for(HandType currentHandType: HandType.values()) {
            if(currentHandType.check(this)) {

                // Assign the best hand
                if(handType == null || currentHandType.getRank() > handType.getRank()) {
                    handType = currentHandType;
                }
            }
        }

        return handType;
    }

    /**
     * Sorts the hand in ascending order
     */
    public void sort() {
        Collections.sort(hand);
    }

    /**
     * @return int Returns the size of the hand
     */
    public int size() {
        return hand.size();
    }

    /**
     * @return String Returns a string representing the hand separated by tabs
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for(int i = 0; i < hand.size() - 1; i++) {
            stringBuilder.append(hand.get(i)).append("\t");
        }

        stringBuilder.append(hand.get(hand.size() - 1));

        return stringBuilder.toString();
    }
}
