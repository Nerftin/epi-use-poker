package Hand;

public class InvalidHandSizeException extends Exception {

    /**
     * Gets thrown if the hand size is larger than the deck size
     *
     * @param message error message
     */
    public InvalidHandSizeException(String message) {
        super(message);
    }
}
