package Hand.Check;

import Hand.Hand;

public class HighCardCheck implements HandCheck {

    /**
     * Any hand is at least a high card
     *
     * @param hand player hand
     * @return boolean true
     */
    @Override
    public boolean check(Hand hand) {
        return true;
    }
}
