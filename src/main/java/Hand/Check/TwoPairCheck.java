package Hand.Check;

import Hand.Hand;

public class TwoPairCheck implements HandCheck {

    /**
     * Two pair is when there are two one pairs
     *
     * @param hand player hand
     * @return boolean Returns true if there is a two pair
     */
    @Override
    public boolean check(Hand hand) {
        boolean onePair = false;

        for (int i : hand.getHandMap().values()) {
            if (i == 2) {
                // If a new pair is found after a pair has already been found
                if (onePair) {
                    return true;
                }

                // A single pair has appeared
                onePair = true;
            }
        }

        return false;
    }
}
