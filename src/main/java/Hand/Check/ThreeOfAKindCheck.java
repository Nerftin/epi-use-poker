package Hand.Check;

import Hand.Hand;

import static Hand.Check.HandCheckUtils.checkNKind;

public class ThreeOfAKindCheck implements HandCheck {

    /**
     * Three pair is when there are three of a card
     *
     * @param hand player hand
     * @return boolean Returns true if there is a three pair
     */
    @Override
    public boolean check(Hand hand) {
        return checkNKind(hand, 3);
    }
}

