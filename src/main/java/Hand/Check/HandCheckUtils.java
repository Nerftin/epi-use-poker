package Hand.Check;

import Cards.Card;
import Cards.Suit;
import Hand.Hand;

/**
 * Useful methods for checking hands
 */
class HandCheckUtils {

    /**
     * Determines if any face has appeared n times in the hand
     *
     * @param n card count
     * @return boolean Returns true if any face appears n times in the hand
     */
    static boolean checkNKind(Hand hand, int n) {
        for (int count : hand.getHandMap().values()) {
            if (count == n) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param hand player hand
     * @return boolean Returns true if the hand is a flush
     */
    static boolean isFlush(Hand hand) {
        Suit suit = hand.getHand().get(0).getSuit();

        // Check to see if all cards have same suit
        for (Card card : hand.getHand()) {
            if (!card.getSuit().equals(suit)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param hand player hand
     * @return boolean Returns true if the hand has a straight
     */
    static boolean isStraight(Hand hand) {
        hand.sort();
        Card previous = hand.getHand().get(0);

        // Check to see if all cards are in ascending order
        for (int i = 1; i < hand.size(); i++) {
            if (hand.getHand().get(i).getFace().getRank() != previous.getFace().getRank() + 1) {
                return false;
            }

            previous = hand.getHand().get(i);
        }

        return true;
    }
}
