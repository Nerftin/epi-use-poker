package Hand.Check;

import Hand.Hand;

import static Hand.Check.HandCheckUtils.checkNKind;

public class OnePairCheck implements HandCheck {

    /**
     * One pair is when there is two of the same card
     *
     * @param hand player hand
     * @return boolean Returns true if card has a one pair
     */
    @Override
    public boolean check(Hand hand) {
        return checkNKind(hand, 2);
    }
}
