package Hand.Check;

import Hand.Hand;

public interface HandCheck {

    /**
     * @param hand Player hand
     * @return Boolean returns true if player hand matches
     */
    boolean check(Hand hand);
}
