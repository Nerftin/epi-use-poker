package Hand.Check;

import Hand.Hand;

import static Hand.Check.HandCheckUtils.isFlush;

public class FlushCheck implements HandCheck {

    /**
     * A flush is when all cards are of the same suit
     *
     * @param hand player hand
     * @return boolean Returns true if the hand has a flush
     */
    @Override
    public boolean check(Hand hand) {
        return isFlush(hand);
    }
}
