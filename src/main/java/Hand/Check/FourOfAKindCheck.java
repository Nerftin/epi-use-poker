package Hand.Check;

import Hand.Hand;

import static Hand.Check.HandCheckUtils.checkNKind;

public class FourOfAKindCheck implements HandCheck {

    /**
     * Four of a kind is when four of same face
     *
     * @param hand player hand
     * @return boolean Returns true if hand has a four of a kind
     */
    @Override
    public boolean check(Hand hand) {
        return checkNKind(hand, 4);
    }
}
