package Hand.Check;

import Hand.Hand;

import static Hand.Check.HandCheckUtils.checkNKind;

public class FullHouseCheck implements HandCheck {

    /**
     * Full house is when hand has a three of a kind and a one pair
     *
     * @param hand player hand
     * @return boolean Returns true if the hand has a full house
     */
    @Override
    public boolean check(Hand hand) {
        return checkNKind(hand, 3) && checkNKind(hand, 2);
    }
}
