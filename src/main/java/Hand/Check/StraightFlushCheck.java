package Hand.Check;

import Hand.Hand;

import static Hand.Check.HandCheckUtils.isFlush;
import static Hand.Check.HandCheckUtils.isStraight;

public class StraightFlushCheck implements HandCheck {

    /**
     * Straight flush is when there is a straight and a flush
     *
     * @param hand player hand
     * @return boolean Returns true if there is a straight flush
     */
    @Override
    public boolean check(Hand hand) {
        return isStraight(hand) && isFlush(hand);
    }
}
