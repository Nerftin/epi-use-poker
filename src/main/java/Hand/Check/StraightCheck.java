package Hand.Check;

import Hand.Hand;

import static Hand.Check.HandCheckUtils.isStraight;

public class StraightCheck implements HandCheck {

    /**
     * Straight is when cards are in sequential order
     *
     * @param hand player hand
     * @return boolean Returns true if the hand has a straight
     */
    @Override
    public boolean check(Hand hand) {
        return isStraight(hand);
    }
}
