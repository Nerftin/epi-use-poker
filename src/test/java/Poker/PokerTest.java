package Poker;

import Cards.Deck;
import Hand.InvalidHandSizeException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PokerTest {

    /**
     * Tests when hand is larger than deck
     */
    @Test
    void testPokerLargeHand() {
        Deck deck = new Deck();

        Exception exception = assertThrows(InvalidHandSizeException.class, () -> new Poker(deck.getDeckLimit() + 1));

        String expectedMessage = "Hand size is larger than deck size";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    /**
     * Tests when hand is smaller than 1
     */
    @Test
    void testPokerSmallHand() {
        Exception exception = assertThrows(InvalidHandSizeException.class, () -> new Poker(0));

        String expectedMessage = "Hand size is smaller than 1";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    /**
     * Tests to see if correct amount of cards is dealt
     *
     * @throws InvalidHandSizeException Shouldn't be thrown since hand is 3
     */
    @Test
    void dealCards() throws InvalidHandSizeException {
        int handSize = 3;

        Poker poker = new Poker(handSize);
        poker.dealCards();

        assertEquals(handSize, poker.getPlayerHand().size());
    }
}