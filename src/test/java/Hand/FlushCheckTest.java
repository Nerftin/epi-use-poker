package Hand;

import Cards.Card;
import Cards.Face;
import Cards.Suit;
import Hand.Check.FlushCheck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FlushCheckTest {

    /**
     * Test if a hand is a flush
     */
    @Test
    void testHasFlush() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.CLUB));
        hand.add(new Card(Face.FOUR, Suit.CLUB));
        hand.add(new Card(Face.SIX, Suit.CLUB));
        hand.add(new Card(Face.JACK, Suit.CLUB));
        hand.add(new Card(Face.ACE, Suit.CLUB));

        assertTrue(new FlushCheck().check(hand));
    }

    /**
     * Test if a hand is not a flush
     */
    @Test
    void testNotHasFlush() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.FOUR, Suit.CLUB));
        hand.add(new Card(Face.SIX, Suit.CLUB));
        hand.add(new Card(Face.JACK, Suit.CLUB));
        hand.add(new Card(Face.ACE, Suit.CLUB));

        assertFalse(new FlushCheck().check(hand));
    }
}