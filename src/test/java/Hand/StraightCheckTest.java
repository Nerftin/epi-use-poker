package Hand;

import Cards.Card;
import Cards.Face;
import Cards.Suit;
import Hand.Check.StraightCheck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StraightCheckTest {

    /**
     * Test if a hand is a Straight
     */
    @Test
    void testHasStraight() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.THREE, Suit.CLUB));
        hand.add(new Card(Face.FIVE, Suit.SPADE));
        hand.add(new Card(Face.FOUR, Suit.CLUB));
        hand.add(new Card(Face.SIX, Suit.HEART));

        assertTrue(new StraightCheck().check(hand));
    }

    /**
     * Test if a hand is not a Straight
     */
    @Test
    void testNotHasStraight() {
        Hand hand = new Hand();
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.FOUR, Suit.CLUB));
        hand.add(new Card(Face.SIX, Suit.CLUB));
        hand.add(new Card(Face.JACK, Suit.CLUB));
        hand.add(new Card(Face.ACE, Suit.CLUB));

        assertFalse(new StraightCheck().check(hand));
    }
}