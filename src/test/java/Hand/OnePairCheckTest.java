package Hand;

import Cards.Card;
import Cards.Face;
import Cards.Suit;
import Hand.Check.OnePairCheck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OnePairCheckTest {

    /**
     * Test to see if a hand has a one pair
     */
    @Test
    void testHasOnePair() {
        Hand hand = new Hand();
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.SIX, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertTrue(new OnePairCheck().check(hand));
    }

    /**
     * Test to see if a hand has a one pair in the middle
     */
    @Test
    void testHasOnePairMiddle() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.THREE, Suit.DIAMOND));
        hand.add(new Card(Face.SIX, Suit.DIAMOND));
        hand.add(new Card(Face.SIX, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertTrue(new OnePairCheck().check(hand));
    }

    /**
     * Test to see if a hand does not have a one pair
     */
    @Test
    void testNotHasOnePair() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.THREE, Suit.DIAMOND));
        hand.add(new Card(Face.SIX, Suit.DIAMOND));
        hand.add(new Card(Face.JACK, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertFalse(new OnePairCheck().check(hand));
    }
}