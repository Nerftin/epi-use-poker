package Hand;

import Cards.Card;
import Cards.Face;
import Cards.Suit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HandTest {

    /**
     * Test to see if a hand has a straight flush
     */
    @Test
    void testHasStraightFlush() {
        Hand hand = new Hand();
        hand.add(new Card(Face.NINE, Suit.DIAMOND));
        hand.add(new Card(Face.SEVEN, Suit.DIAMOND));
        hand.add(new Card(Face.SIX, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertEquals(HandType.STRAIGHT_FLUSH, hand.checkHandType());
    }

    /**
     * Test to see if a hand is four of a kind
     */
    @Test
    void testHasFourOfAKind() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.TWO, Suit.HEART));
        hand.add(new Card(Face.TWO, Suit.CLUB));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertEquals(HandType.FOUR_OF_A_KIND, hand.checkHandType());
    }

    /**
     * Test to see if a hand is a full house
     */
    @Test
    void testFullHouse() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.HEART));
        hand.add(new Card(Face.EIGHT, Suit.CLUB));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertEquals(HandType.FULL_HOUSE, hand.checkHandType());
    }

    /**
     * Test if a hand is a flush
     */
    @Test
    void testHasFlush() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.CLUB));
        hand.add(new Card(Face.FOUR, Suit.CLUB));
        hand.add(new Card(Face.SIX, Suit.CLUB));
        hand.add(new Card(Face.JACK, Suit.CLUB));
        hand.add(new Card(Face.ACE, Suit.CLUB));

        assertEquals(HandType.FLUSH, hand.checkHandType());
    }

    /**
     * Test if a hand is a Straight
     */
    @Test
    void testHasStraight() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.THREE, Suit.CLUB));
        hand.add(new Card(Face.FIVE, Suit.SPADE));
        hand.add(new Card(Face.FOUR, Suit.CLUB));
        hand.add(new Card(Face.SIX, Suit.HEART));

        assertEquals(HandType.STRAIGHT, hand.checkHandType());
    }

    /**
     * Test to see if a hand has three of a kind
     */
    @Test
    void testHasThreeOfAKind() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.TWO, Suit.HEART));
        hand.add(new Card(Face.JACK, Suit.CLUB));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertEquals(HandType.THREE_OF_A_KIND, hand.checkHandType());
    }

    /**
     * Test to see if a hand has a two pair
     */
    @Test
    void testHasTwoPair() {
        Hand hand = new Hand();
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.ACE, Suit.CLUB));
        hand.add(new Card(Face.TEN, Suit.HEART));
        hand.add(new Card(Face.TEN, Suit.CLUB));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertEquals(HandType.TWO_PAIR, hand.checkHandType());
    }

    /**
     * Test to see if a hand has a one pair
     */
    @Test
    void testHasOnePair() {
        Hand hand = new Hand();
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.ACE, Suit.CLUB));
        hand.add(new Card(Face.SIX, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.HEART));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertEquals(HandType.ONE_PAIR, hand.checkHandType());
    }

    /**
     * Test to see if hand is a high cards
     */
    @Test
    void testHighCards() {
        Hand hand = new Hand();
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.FOUR, Suit.CLUB));
        hand.add(new Card(Face.SIX, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.HEART));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertEquals(HandType.HIGH_CARDS, hand.checkHandType());
    }

    /**
     * Test hand's to string method
     */
    @Test
    void testToString() {
        Hand hand = new Hand();
        hand.add(new Card(Face.SEVEN, Suit.SPADE));
        hand.add(new Card(Face.NINE, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.HEART));
        hand.add(new Card(Face.QUEEN, Suit.HEART));
        hand.add(new Card(Face.ACE, Suit.DIAMOND));

        assertEquals("7♠\t9♦\t10♥\tQ♥\tA♦", hand.toString());
    }
}