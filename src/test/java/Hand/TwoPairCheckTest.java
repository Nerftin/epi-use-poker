package Hand;

import Cards.Card;
import Cards.Face;
import Cards.Suit;
import Hand.Check.TwoPairCheck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TwoPairCheckTest {

    /**
     * Test to see if a hand has a two pair
     */
    @Test
    void testHasTwoPair() {
        Hand hand = new Hand();
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertTrue(new TwoPairCheck().check(hand));
    }

    /**
     * Test to see if a hand has a two pair in the middle
     */
    @Test
    void testHasTwoPairMiddle() {
        Hand hand = new Hand();
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertTrue(new TwoPairCheck().check(hand));
    }

    /**
     * Test to see if a hand does not have a two pair
     */
    @Test
    void testNotHasTwoPair() {
        Hand hand = new Hand();
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.ACE, Suit.DIAMOND));
        hand.add(new Card(Face.FIVE, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertFalse(new TwoPairCheck().check(hand));
    }
}