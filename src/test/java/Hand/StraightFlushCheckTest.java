package Hand;

import Cards.Card;
import Cards.Face;
import Cards.Suit;
import Hand.Check.StraightCheck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StraightFlushCheckTest {

    /**
     * Test to see if a hand has a straight flush
     */
    @Test
    void testHasStraightFlush() {
        Hand hand = new Hand();
        hand.add(new Card(Face.NINE, Suit.DIAMOND));
        hand.add(new Card(Face.SEVEN, Suit.DIAMOND));
        hand.add(new Card(Face.SIX, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertTrue(new StraightCheck().check(hand));
    }

    /**
     * Test to see if a hand with a straight but not a flush is a royal flush
     */
    @Test
    void testHasStraightFlushNotSuit() {
        Hand hand = new Hand();
        hand.add(new Card(Face.NINE, Suit.DIAMOND));
        hand.add(new Card(Face.SEVEN, Suit.CLUB));
        hand.add(new Card(Face.SIX, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertTrue(new StraightCheck().check(hand));
    }

    /**
     * Test to see if a hand with a flush but not a straight is a royal flush
     */
    @Test
    void testHasStraightFlushNotFace() {
        Hand hand = new Hand();
        hand.add(new Card(Face.NINE, Suit.DIAMOND));
        hand.add(new Card(Face.SEVEN, Suit.CLUB));
        hand.add(new Card(Face.FIVE, Suit.DIAMOND));
        hand.add(new Card(Face.TEN, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertFalse(new StraightCheck().check(hand));
    }
}