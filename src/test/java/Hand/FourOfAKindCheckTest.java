package Hand;

import Cards.Card;
import Cards.Face;
import Cards.Suit;
import Hand.Check.FourOfAKindCheck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FourOfAKindCheckTest {

    /**
     * Test to see if a hand is four of a kind
     */
    @Test
    void testHasFourOfAKind() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.TWO, Suit.HEART));
        hand.add(new Card(Face.TWO, Suit.CLUB));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertTrue(new FourOfAKindCheck().check(hand));
    }

    /**
     * Test to see if a hand has four of a kind at the end of the hand
     */
    @Test
    void testHasFourOfAKindEnd() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.JACK, Suit.DIAMOND));
        hand.add(new Card(Face.JACK, Suit.HEART));
        hand.add(new Card(Face.JACK, Suit.CLUB));
        hand.add(new Card(Face.JACK, Suit.DIAMOND));

        assertTrue(new FourOfAKindCheck().check(hand));
    }

    /**
     * Test to see if a hand does not have four of a kind
     */
    @Test
    void testNotHasFourOfAKind() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.FIVE, Suit.DIAMOND));
        hand.add(new Card(Face.TWO, Suit.HEART));
        hand.add(new Card(Face.JACK, Suit.CLUB));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertFalse(new FourOfAKindCheck().check(hand));
    }
}