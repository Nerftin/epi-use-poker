package Hand;

import Cards.Card;
import Cards.Face;
import Cards.Suit;
import Hand.Check.ThreeOfAKindCheck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ThreeOfAKindCheckTest {

    /**
     * Test to see if a hand has three of a kind
     */
    @Test
    void testHasThreeOfAKind() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.TWO, Suit.HEART));
        hand.add(new Card(Face.JACK, Suit.CLUB));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertTrue(new ThreeOfAKindCheck().check(hand));
    }

    /**
     * Test to see if a hand has three of a kind in the middle
     */
    @Test
    void testHasThreeOfAKindMiddle() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.JACK, Suit.DIAMOND));
        hand.add(new Card(Face.JACK, Suit.HEART));
        hand.add(new Card(Face.JACK, Suit.CLUB));
        hand.add(new Card(Face.KING, Suit.DIAMOND));

        assertTrue(new ThreeOfAKindCheck().check(hand));
    }

    /**
     * Test to see if a hand has three of a kind at the end
     */
    @Test
    void testHasThreeOfAKindEnd() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.FIVE, Suit.DIAMOND));
        hand.add(new Card(Face.KING, Suit.HEART));
        hand.add(new Card(Face.KING, Suit.CLUB));
        hand.add(new Card(Face.KING, Suit.DIAMOND));

        assertTrue(new ThreeOfAKindCheck().check(hand));
    }

    /**
     * Test to see if a hand is not three of a kind
     */
    @Test
    void testNotHasThreeOfAKind() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.FIVE, Suit.DIAMOND));
        hand.add(new Card(Face.TWO, Suit.HEART));
        hand.add(new Card(Face.JACK, Suit.CLUB));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertFalse(new ThreeOfAKindCheck().check(hand));
    }
}