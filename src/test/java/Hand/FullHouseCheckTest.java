package Hand;

import Cards.Card;
import Cards.Face;
import Cards.Suit;
import Hand.Check.FullHouseCheck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FullHouseCheckTest {

    /**
     * Test to see if a hand is a full house
     */
    @Test
    void testHasFullHouse() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.EIGHT, Suit.HEART));
        hand.add(new Card(Face.EIGHT, Suit.CLUB));
        hand.add(new Card(Face.EIGHT, Suit.DIAMOND));

        assertTrue(new FullHouseCheck().check(hand));
    }

    /**
     * Test to see if a hand is a full house with three matching cards first
     */
    @Test
    void testHasFullHouseThreeFirst() {
        Hand hand = new Hand();
        hand.add(new Card(Face.EIGHT, Suit.HEART));
        hand.add(new Card(Face.EIGHT, Suit.CLUB));
        hand.add(new Card(Face.TWO, Suit.DIAMOND));
        hand.add(new Card(Face.TWO, Suit.SPADE));
        hand.add(new Card(Face.TWO, Suit.DIAMOND));

        assertTrue(new FullHouseCheck().check(hand));
    }

    /**
     * Test to see if hand is not a full house
     */
    @Test
    void testNotHasFullHouse() {
        Hand hand = new Hand();
        hand.add(new Card(Face.SEVEN, Suit.SPADE));
        hand.add(new Card(Face.SIX, Suit.DIAMOND));
        hand.add(new Card(Face.SEVEN, Suit.HEART));
        hand.add(new Card(Face.SEVEN, Suit.CLUB));
        hand.add(new Card(Face.KING, Suit.DIAMOND));

        assertFalse(new FullHouseCheck().check(hand));
    }
}