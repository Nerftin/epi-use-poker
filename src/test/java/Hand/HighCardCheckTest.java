package Hand;

import Cards.Card;
import Cards.Face;
import Cards.Suit;
import Hand.Check.HighCardCheck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HighCardCheckTest {

    /**
     * Test to see if hand is a high cards
     */
    @Test
    void check() {
        Hand hand = new Hand();
        hand.add(new Card(Face.TWO, Suit.CLUB));

        assertTrue(new HighCardCheck().check(hand));
    }
}