package Cards;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DeckTest {

    /**
     * Test to see if drawing a card removes it from the deck
     */
    @Test
    void drawCard() {
        Deck deck = new Deck();
        Card draw = deck.drawCard();

        boolean contains = false;

        // Check if deck contains card
        for(Card c: deck.getDeck()) {
            if (c.equals(draw) && c.getSuit().getSymbol() == draw.getSuit().getSymbol()) {
                contains = true;
                break;
            }
        }

        assertFalse(contains);
    }

    /**
     * Test to see if deck size is unchanged after shuffle
     */
    @Test
    void testShuffleSameSize() {
        Deck deck = new Deck();
        deck.shuffle();

        ArrayList<Card> deckArrayList = new ArrayList<>(deck.getDeckLimit());

        // Create a card for every combination of faces and suites
        // and add it to the deck
        for(Face face: Face.values()) {
            for(Suit suit: Suit.values()) {
                deckArrayList.add(new Card(face, suit));
            }
        }

        assertTrue(deck.size() == deckArrayList.size());
    }
}