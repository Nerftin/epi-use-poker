package Cards;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {

    /**
     * Tests to see if a card equals itself
     */
    @Test
    void testEquals() {
        Card a = new Card(Face.ACE, Suit.DIAMOND);
        assertEquals(a, a);
    }

    /**
     * Tests to see if a card equals same card
     */
    @Test
    void testEqualsType() {
        Card a = new Card(Face.ACE, Suit.DIAMOND);
        Card b = new Card(Face.ACE, Suit.DIAMOND);

        assertEquals(a, b);
    }

    /**
     * Tests to see if a card equals same face ignoring suit
     */
    @Test
    void testEqualsRank() {
        Card a = new Card(Face.ACE, Suit.DIAMOND);
        Card b = new Card(Face.ACE, Suit.SPADE);

        assertEquals(a, b);
    }

    /**
     * Tests to see if a card equals a different card
     */
    @Test
    void testNotEquals() {
        Card a = new Card(Face.ACE, Suit.DIAMOND);
        Card b = new Card(Face.TWO, Suit.SPADE);

        assertNotEquals(a, b);
    }

    /**
     * Tests to see if a card equals a card with same suit different face
     */
    @Test
    void testNotEqualsFace() {
        Card a = new Card(Face.ACE, Suit.DIAMOND);
        Card b = new Card(Face.TWO, Suit.DIAMOND);

        assertNotEquals(a, b);
    }

    /**
     * Tests to see if a card equals null
     */
    @Test
    void testEqualsNull() {
        Card a = new Card(Face.ACE, Suit.DIAMOND);

        assertNotEquals(a, null);
    }

    /**
     * Tests to see if a card equals a different class
     */
    @Test
    void testEqualsClass() {
        Card a = new Card(Face.ACE, Suit.DIAMOND);
        Suit d = Suit.DIAMOND;

        assertNotEquals(a, d);
    }

    /**
     * Test to see if toString works correctly
     */
    @Test
    void testToString() {
        Card a = new Card(Face.ACE, Suit.DIAMOND);
        assertEquals("A♦", a.toString());
    }
}